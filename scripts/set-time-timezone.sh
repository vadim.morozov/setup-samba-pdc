#!/bin/bash
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime
timedatectl set-local-rtc true
timedatectl set-ntp true
