#!/bin/bash

SERVICES="NetworkManager"

systemctl stop $SERVICES
systemctl disable $SERVICES
systemctl mask $SERVICES
