#!/bin/bash

export PASSWORD_FILE=$1
export PASSWORD_LENGTH=8

export PASSWORD_GEN_COMMAND="pwgen -s $PASSWORD_LENGTH 1"

cat > $PASSWORD_FILE <<EOF
# Generated passwords
export PASSWORD_LOCAL_ADMIN="`$PASSWORD_GEN_COMMAND`"
export PASSWORD_DOMAIN_ADMIN="`$PASSWORD_GEN_COMMAND`"
export PASSWORD_VADIM="`$PASSWORD_GEN_COMMAND`"
export PASSWORD_IRINA="`$PASSWORD_GEN_COMMAND`"
export PASSWORD_DMITRY="`$PASSWORD_GEN_COMMAND`"
export PASSWORD_FEDOR="`$PASSWORD_GEN_COMMAND`"
EOF