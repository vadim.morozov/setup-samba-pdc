#!/bin/bash

name=$1
ip=$2
password=$3

ip_last=${ip##*.}
name_first=$(echo $name | cut -d. -f1)

samba-tool dns add dc0 1.168.192.in-addr.arpa $ip_last PTR $name. --password $password
samba-tool dns add dc0 vm.internal $name_first A $ip --password $password

