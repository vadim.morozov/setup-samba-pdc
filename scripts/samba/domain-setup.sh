#!/bin/bash


samba-tool domain provision \
    --use-rfc2307 \
    --realm=VM.INTERNAL \
    --domain=VM \
    --host-name=DC0 \
    --dns-backend=BIND9_DLZ \
    --server-role=dc \
    --adminpass=$PASSWORD_DOMAIN_ADMIN \
    --option="interfaces = lo eth0" \
    --option="bind interfaces only = yes" \
    --option="username map = /etc/samba/usermap"

samba_upgradedns --dns-backend=BIND9_DLZ

systemctl restart named

