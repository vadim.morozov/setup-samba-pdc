#!/bin/bash

systemctl stop samba-ad-dc
systemctl stop smbd
systemctl stop nmdb
systemctl mask smbd
systemctl mask nmdb

killall -9 samba
killall -9 smbd
killall -9 nmbd

rm -f /etc/samba/smb.conf
rm -f -r /var/lib/samba/bind-dns
rm -f -r /var/lib/samba/private
rm -f -r /var/lib/samba/sysvol
