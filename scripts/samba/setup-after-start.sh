#!/bin/bash

rm -f /var/log/samba/log.*

add_static_dns_record() {
    name=$1
    ip=$2
    password=$3

    ip_last=${ip##*.}
    name_first=$(echo $name | cut -d. -f1)

    samba-tool dns add dc0 1.168.192.in-addr.arpa $ip_last PTR $name. --password $PASSWORD_DOMAIN_ADMIN
    samba-tool dns add dc0 vm.internal $name_first A $ip --password $PASSWORD_DOMAIN_ADMIN
}


samba-tool user create dhcpduser --description="Unprivileged user for TSIG-GSSAPI DNS updates via ISC DHCP server" --random-password
samba-tool user setexpiry dhcpduser --noexpiry
samba-tool group addmembers DnsAdmins dhcpduser
samba-tool user setexpiry administrator --noexpiry

samba-tool domain exportkeytab --principal=dhcpduser@VM /etc/dhcpduser.keytab
chown dhcpd:root /etc/dhcpduser.keytab
chmod 400 /etc/dhcpduser.keytab
chown dhcpd:root /tmp/dhcp-dyndns.cc

echo $PASSWORD_DOMAIN_ADMIN | kinit Administrator@VM.INTERNAL

samba-tool dns zonecreate dc0.vm.internal 1.168.192.in-addr.arpa --use-kerberos=required
samba-tool dns zonelist DC0 --use-kerberos=required --reverse

# Add static dns entries
# samba-tool dns add <server> <zone> <name> <A|AAAA|PTR|CNAME|NS|MX|SRV|TXT> <data>

samba-tool dns add dc0 1.168.192.in-addr.arpa 100 PTR SERVER.VM.INTERNAL. --password soc4qHEd
