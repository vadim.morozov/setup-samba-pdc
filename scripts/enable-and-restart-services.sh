#!/bin/bash

export SERVICES="chrony ssh isc-dhcp-server named"

systemctl unmask $SERVICES
systemctl enable $SERVICES
systemctl restart $SERVICES


