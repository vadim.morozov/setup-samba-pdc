#!/bin/bash

# Remove extra packages
apt remove alsa-utils bluetooth aptitude zsh dnsmasq \
	gcc gcc cpp g++ make \
	docker docker-ce docker-ce-cli containerd.io \
	fontconfig wireless-tools isc-dhcp-client

apt apt autoremove

# Remove not needed files/directories

rm -f -r /usr/share/fonts
rm -f -r /etc/bluetooth
rm -f -r /etc/NetworkManager
rm -f -r /etc/X11

# Install new packages

apt update
apt upgrade

apt install netplan.io isc-dhcp-server bind9 samba winbind pwgen chrony
