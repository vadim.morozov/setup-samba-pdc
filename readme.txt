+. ���������� ����� �� ��-����� � ������� usbimager.exe

���� ������: Orangepizero3_1.0.0_ubuntu_jammy_server_linux6.1.31.img

+. ���������� �������, ���������� � ��-�����
������ � sudo -i (orangepi/orangepi)

+. ��������� /etc/hosts

+. ��������� /etc/ssh/sshd_config:

+. ������� sshd
systemctl restart sshd

+. ��������� /etc/netplan/orangepi-default.yaml:

+. ��������� netplan apply, �� ������ ���� ������
���������, ��� IP �����, ifconfig ������ ������ ������������� IP:

inet 192.168.1.10  netmask 255.255.255.0  broadcast 192.168.1.255

+. ��������� ���������� � �������, ������ �� ssh

+. ��������� ��� ������

apt update
apt upgrade

+. ������� �������� ������

apt remove alsa-utils bluetooth aptitude zsh dnsmasq perl python3 \
gcc gcc cpp g++ make \
docker-ce docker-ce-cli containerd.io \
fontconfig wireless-tools chrony isc-dhcp-client

apt autoremove

rm -f -r /usr/share/fonts
rm -f -r /etc/bluetooth
rm -f -r /etc/NetworkManager
rm -f -r /etc/X11

+. ������ ������

apt install netplan.io isc-dhcp-server bind9 samba winbind

+. ��������� ������������ localAdmin � ��� �� � ������ sudo

useradd localAdmin && passwd localAdmin
=>uuAi8FFcI0
groupmems -g sudo -a localAdmin

+. ��������� ������������ orangepi

passwd -l orangepi

+. ��������� /etc/dhcpd.conf
+. ��������� /usr/local/bin/dhcp-dyndns.sh
chmod +x /usr/local/bin/dhcp-dyndns.sh

+. �������� ��� ���������

systemctl enable isc-dhcp-server && systemctl restart isc-dhcp-server
systemctl enable named && systemctl restart named

chmod +x domainProvision.sh

systemctl enable samba-ad-dc && systemctl restart samba-ad-dc



