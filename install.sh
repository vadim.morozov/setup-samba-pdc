#!/bin/bash

# Set .sh script as executable
find . -type f -name '*.sh' -exec chmod +x {} \;

mkdir tmp
export PASSWORD_FILE=tmp/passwords.sh

scripts/generate-passwords.sh $PASSWORD_FILE
chmod +x $PASSWORD_FILE

. ./$PASSWORD_FILE

./scripts/package-manipulations.sh
./scripts/set-time-timezone.sh


./scripts/disable-services.sh

# Copy configuratuion files
cp -f -r root/* /

# Enable and restart services
./scripts/enable-and-restart-services.sh

package-manipulations.sh  samba  set-time-timezone.sh

# Linux user manipulations
crypted_password=$(perl -e "print crypt('$PASSWORD_LOCAL_ADMIN',\"$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 32)sa\");")
echo New password for localAdmin is $crypted_password
useradd localAdmin -p "$crypted_password"
echo User added
echo $PASSWORD_LOCAL_ADMIN | groupmems -g sudo -a localAdmin
passwd -l orangepi

# samba AD/DC setup
./scripts/samba/stop-and-clean.sh
./scripts/samba/domain-setup.sh
./scripts/samba/enable-and-start.sh
./scripts/samba/setup-after-start.sh

# Add static dns entries
./scripts/samba/add_static_dns_record.sh GATE.VM.INTERNAL 192.168.1.2 $PASSWORD_DOMAIN_ADMIN
./scripts/samba/add_static_dns_record.sh DC0.VM.INTERNAL 192.168.1.10 $PASSWORD_DOMAIN_ADMIN
./scripts/samba/add_static_dns_record.sh SERVER.VM.INTERNAL 192.168.1.100 $PASSWORD_DOMAIN_ADMIN

# Add domain users and groups
samba-tool user add vadim "$PASSWORD_VADIM"
samba-tool user add irina "$PASSWORD_IRINA"
samba-tool user add dmitry "$PASSWORD_DMITRY"
samba-tool user add fedor "$PASSWORD_FEDOR"

samba-tool group add children
samba-tool group add adults

samba-tool group addmembers children dmitry,fedor
samba-tool group addmembers adults vadim,irina

echo ==================================================
echo New Passwords:
echo "      localAdmin: $PASSWORD_LOCAL_ADMIN"
echo "   administrator: $PASSWORD_DOMAIN_ADMIN"
echo ==================================================
echo "           vadim: $PASSWORD_VADIM"
echo "           irina: $PASSWORD_IRINA"
echo "          dmitry: $PASSWORD_DMITRY"
echo "           fedor: $PASSWORD_FEDOR"
echo ==================================================

