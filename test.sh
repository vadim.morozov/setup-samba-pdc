#!/bin/bash

command_with_echo() {
    echo "====> $*"
    $*
    rc=$?
    if [ $rc -ne 0 ]; then
      echo "Return code is $rc"
    fi
}


command_with_echo host -t SRV _ldap._tcp.vm.internal
command_with_echo host -t SRV _kerberos._tcp.vm.internal
command_with_echo host -t SRV _kerberos._udp.vm.internal
command_with_echo klist -e
